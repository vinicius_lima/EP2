# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

-1. Utilização do programa
     -Utilizando o software NetBeans, compile o programa e aperte play.
	-1.1 Janela Login
		-faça um cadastro clicando em 'cadastrar' e forneça seu id e senhas cadastradas;
		-caso não queira se cadastrar, por padrão, existe um cadastro com id 'teste' e senha 'teste';
	-1.2 Janela Principal
		-Os produtos cadastrados serão exibidos na tabela;
		-Realize várias operações clicando em algum botão;

-2. Janelas
	-2.1 Janela Venda
		-selecione um produto e digite a quantidade, depois clique em 'Adicionar no carrinho'. Repita quantas vezes for necessário;
		-Digite o nome do cliente;
		-selecione a mesa e a forma de pagamento;
		-Digite observações feitas pelo Cliente caso exista;
		-clique em 'finalizar pedido' para salvar as informações;
	-2.2 Janela Relatório de Vendas
		-selecione o ano;
		-selecione o mês;
		-selecione o dia;
		-clique em 'pesquisar';
	-2.3 Janela Cardápio
		-Clique 2 vezes na cédula do item que deseja alterar;
		-para adicionar itens clique em 'Adicionar item' e uma linha vazia aparecerá na tabela;
		-para remover itens selecione as linhas dos itens que deseja remover e clique em 'remover itens';
		-ao cliquar em voltar as iformações serão salvas no arquivo 'cardapios.txt';
	-2.4 Janela Troca Senha
		-digite a nova senha e confirme-a digitando novamento no campo 'confirmar senha';
		-clique em 'OK' para salvar alteração ou 'cancelar para sair';
	-2.5 Janela Cadastro
		-preencha todos os campos solicitados para se cadastrar e clique em 'ok';
