package view;

import controllers.JanelaTrocaSenhaClickListener;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import model.usuarios.Funcionario;

public class JanelaTrocaSenha extends View{
    private Funcionario funcionario;
    private JPanel painel;
    private JLabel digitarSenhaLabel, confirmaLabel, tituloLabel;
    private JPasswordField senhaText, confirmaText;
    private JButton okButton, cancelarButton;
    
    public JanelaTrocaSenha(Funcionario funcionario){
        this.funcionario = funcionario;
        preparaJanela();
    }
    public void preparaJanela(){
        setMainFrame(new JFrame("Trocar senha"));
        getMainFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GridBagConstraints restricoes = new GridBagConstraints();
        painel = new JPanel(new GridBagLayout());
        tituloLabel = new JLabel("Forneça sua nova senha");
        digitarSenhaLabel = new JLabel("Senha:");
        confirmaLabel = new JLabel("Confirmar senha:");
        senhaText = new JPasswordField();
        confirmaText = new JPasswordField();
        okButton = new JButton("OK");
        cancelarButton = new JButton("Cancelar");
        
        restricoes.gridx = 2;
        restricoes.gridy = 0;
        restricoes.gridwidth = 5;
        restricoes.gridheight = 1;
        restricoes.anchor = GridBagConstraints.CENTER;
        restricoes.insets = new Insets(10, 10, 5, 10);
        painel.add(tituloLabel, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 1;
        restricoes.gridwidth = 2;
        restricoes.gridheight = 1;
        restricoes.anchor = GridBagConstraints.WEST;
        restricoes.insets = new Insets(5, 5, 5, 5);
        painel.add(digitarSenhaLabel, restricoes);
        
        restricoes.gridx = 3;
        restricoes.gridy = 1;
        restricoes.gridwidth = 10;
        restricoes.gridheight = 1;
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.anchor = GridBagConstraints.CENTER;
        restricoes.insets = new Insets(5, 5, 5, 5);
        senhaText.setColumns(25);
        painel.add(senhaText, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 2;
        restricoes.gridwidth = 3;
        restricoes.gridheight = 1;
        restricoes.anchor = GridBagConstraints.EAST;
        restricoes.insets = new Insets(5, 5, 5, 5);
        painel.add(confirmaLabel, restricoes);
        
        restricoes.gridx = 4;
        restricoes.gridy = 2;
        restricoes.gridwidth = 10;
        restricoes.gridheight = 1;
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.anchor = GridBagConstraints.CENTER;
        restricoes.insets = new Insets(5, 5, 5, 5);
        confirmaText.setColumns(25);
        painel.add(confirmaText, restricoes);
        
        restricoes.gridx = 5;
        restricoes.gridy = 3;
        restricoes.gridwidth = 1;
        restricoes.gridheight = 1;
        restricoes.anchor = GridBagConstraints.CENTER;
        restricoes.insets = new Insets(5, 5, 5, 5);
        painel.add(okButton, restricoes);
        
        restricoes.gridx = 6;
        restricoes.gridy = 3;
        restricoes.gridwidth = 1;
        restricoes.gridheight = 1;
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.anchor = GridBagConstraints.CENTER;
        restricoes.insets = new Insets(5, 5, 10, 5);
        painel.add(cancelarButton, restricoes);
        
        getMainFrame().add(painel);
        getMainFrame().pack();
        getMainFrame().setLocation(540, 350);
        getMainFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        getMainFrame().setVisible(true);
        controles();
    }
    public void controles(){
        okButton.setActionCommand("OK");
        cancelarButton.setActionCommand("cancelar");
        
        okButton.addActionListener(new JanelaTrocaSenhaClickListener(senhaText, confirmaText, funcionario, getMainFrame()));
        cancelarButton.addActionListener(new JanelaTrocaSenhaClickListener(getMainFrame()));
    }
}
