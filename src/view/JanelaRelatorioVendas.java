package view;

import controllers.JanelaRelatorioVendasClickListener;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.usuarios.Funcionario;

public class JanelaRelatorioVendas extends View{
    private JPanel mainPanel, imagemPanel, usuarioPanel, opcoesPanel, tabelaPanel, anoPanel, mesAnoPanel, diasMesPanel, buttonPanel;
    private JLabel usuarioLabel, dataLabel, mesLabel, diaLabel, anoLabel, imagemLabel;
    private Date data;
    private JComboBox mesesDoAno, diasDoMes, anoComboBox;
    private JButton pesquisarButton, voltarButton;
    private JScrollPane tabelaVendasPanel;
    private JTable tabelaVendas;
    private DefaultTableModel modelo;
    private Funcionario funcionario;
    private GridBagConstraints restricoes;
    private Calendar calendario;

    public JanelaRelatorioVendas(Funcionario funcionario, JFrame mainFrame) {
        this.funcionario = funcionario;
        setMainFrame(mainFrame);
        preparaGUI();
    }
    public void preparaGUI(){
        //Panels
        mainPanel = new JPanel(new GridBagLayout());
        imagemPanel = new JPanel();
        usuarioPanel = new JPanel(new GridBagLayout());
        opcoesPanel = new JPanel(new GridLayout(4, 1));
        anoPanel = new JPanel(new FlowLayout());
        mesAnoPanel = new JPanel(new FlowLayout());
        diasMesPanel = new JPanel(new FlowLayout());
        buttonPanel = new JPanel(new FlowLayout());
        tabelaPanel = new JPanel();
        //Data
        data = new Date();
        //Labels
        usuarioLabel = new JLabel("Funcionário: " + funcionario.getNome());
        dataLabel = new JLabel("Data: " + new SimpleDateFormat("dd/MM/yyyy").format(data));
        mesLabel = new JLabel("Mês: ");
        diaLabel = new JLabel("Dia: ");
        anoLabel = new JLabel("Ano: ");
        imagemLabel = new JLabel();
        imagemLabel.setIcon(new ImageIcon("default_files/imagem_padrao2.png"));
        //JComboBox
        mesesDoAno = new JComboBox();
        diasDoMes = new JComboBox();
        anoComboBox = new JComboBox();
        anoComboBox.addItem("");
        anoComboBox.addItem("2016");
        anoComboBox.addItem("2015");
        anoComboBox.addItem("2014");
        diasDoMes.addItem("");
        mesesDoAno.addItem("");
        mesesDoAno.addItem("Janeiro");
        mesesDoAno.addItem("Fevereiro");
        mesesDoAno.addItem("Março");
        mesesDoAno.addItem("Abril");
        mesesDoAno.addItem("Maio");
        mesesDoAno.addItem("Junho");
        mesesDoAno.addItem("Julho");
        mesesDoAno.addItem("Agosto");
        mesesDoAno.addItem("Setembro");
        mesesDoAno.addItem("Outubro");
        mesesDoAno.addItem("Novembro");
        mesesDoAno.addItem("Dezembro");
        //GridBagConstrainst
        restricoes = new GridBagConstraints();
        //Buttons
        pesquisarButton = new JButton("Pesquisar");
        voltarButton = new JButton("Voltar");
        //calendario
        calendario = new GregorianCalendar();
        //tabela
        modelo = new DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Mesa", "Nome Cliente", "Nome Funcionário", "Data", "Hora", "Pagamento", "Valor Total", "Observações do cliente"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        };
        tabelaVendas = new JTable(modelo);
        tabelaVendasPanel = new JScrollPane(tabelaVendas);
        tabelaVendasPanel.setPreferredSize(new Dimension(900, 300));
        
        mesesDoAno.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                int ano = 0;
                if(anoComboBox.getSelectedItem().toString().equals("2016")){
                    ano = 116;
                } else if(anoComboBox.getSelectedItem().toString().equals("2015")){
                    ano = 115;
                } else if(anoComboBox.getSelectedItem().toString().equals("2014")){
                    ano = 114;
                } else if(anoComboBox.getSelectedItem().toString().equals("")){
                    return;
                }
                int mes = 0;
                int quantidadeDeDias = 0;
                if(mesesDoAno.getSelectedItem().toString().equals("Janeiro")){
                    mes = 0;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Fevereiro")){
                    mes = 1;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Março")){
                    mes = 2;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Abril")){
                    mes = 3;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Maio")){
                    mes = 4;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Junho")){
                    mes = 5;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Julho")){
                    mes = 6;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Agosto")){
                    mes = 7;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Setembro")){
                    mes = 8;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Outubro")){
                    mes  = 9;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Novembro")){
                    mes  = 10;
                } else if(mesesDoAno.getSelectedItem().toString().equals("Dezembro")){
                    mes = 11;
                } else if(mesesDoAno.getSelectedItem().toString().equals("")){
                    return;
                }
                calendario.set(ano, mes, 1);
                diasDoMes.removeAllItems();
                quantidadeDeDias = calendario.getActualMaximum(Calendar.DAY_OF_MONTH);
                for(int i = 1; i<=quantidadeDeDias; i++){
                    diasDoMes.addItem(i);
                }
            }
        });
        imagemPanel.add(imagemLabel);
        
        restricoes.gridx = 0;
        restricoes.gridy = 0;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 12;
        restricoes.insets = new Insets(10, 10, 10, 10);
        restricoes.fill = GridBagConstraints.NONE;
        mainPanel.add(imagemPanel, restricoes);
        imagemPanel.setVisible(true);
        
        restricoes.gridx = 0;
        restricoes.gridy = 0;
        restricoes.gridwidth = 10;
        restricoes.gridheight = 1;
        restricoes.insets = new Insets(10, 10, 10, 10);
        restricoes.anchor = GridBagConstraints.WEST;
        usuarioPanel.add(usuarioLabel, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 1;
        restricoes.gridwidth = 10;
        restricoes.gridheight = 1;
        restricoes.insets = new Insets(10, 10, 0, 10);
        restricoes.anchor = GridBagConstraints.WEST;
        usuarioPanel.add(dataLabel, restricoes);
        
        restricoes.gridx = 12;
        restricoes.gridy = 0;
        restricoes.gridwidth = 28;
        restricoes.gridheight = 6;
        restricoes.insets = new Insets(10, 20, 10, 10);
        restricoes.fill = GridBagConstraints.NONE;
        mainPanel.add(usuarioPanel, restricoes);
        usuarioPanel.setVisible(true);
        
        diasMesPanel.add(diaLabel);
        diasMesPanel.add(diasDoMes);
        mesAnoPanel.add(mesLabel);
        mesAnoPanel.add(mesesDoAno);
        buttonPanel.add(pesquisarButton);
        buttonPanel.add(voltarButton);
        anoPanel.add(anoLabel);
        anoPanel.add(anoComboBox);
        
        opcoesPanel.add(diasMesPanel);
        opcoesPanel.add(mesAnoPanel);
        opcoesPanel.add(anoPanel);
        opcoesPanel.add(buttonPanel);
        
        restricoes.gridx = 12;
        restricoes.gridy = 6;
        restricoes.gridwidth = 28;
        restricoes.gridheight = 6;
        restricoes.insets = new Insets(10, 20, 10, 10);
        restricoes.fill = GridBagConstraints.NONE;
        mainPanel.add(opcoesPanel, restricoes);
        opcoesPanel.setVisible(true);
        
        tabelaPanel.add(tabelaVendasPanel);
        
        restricoes.gridx = 0;
        restricoes.gridy = 13;
        restricoes.gridwidth = 40;
        restricoes.gridheight = 30;
        restricoes.insets = new Insets(30, 10, 10, 10);
        restricoes.fill = GridBagConstraints.NONE;
        mainPanel.add(tabelaPanel, restricoes);
        tabelaPanel.setVisible(true);
        
        getMainFrame().add(mainPanel);
        getMainFrame().setVisible(true);
        controles();
    }
    public void controles(){
        voltarButton.setActionCommand("voltar");
        pesquisarButton.setActionCommand("pesquisar");
        
        pesquisarButton.addActionListener(new JanelaRelatorioVendasClickListener(mesesDoAno, diasDoMes, anoComboBox, modelo));
        voltarButton.addActionListener(new JanelaRelatorioVendasClickListener(getMainFrame(), mainPanel, funcionario));
    }
}