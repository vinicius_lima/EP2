package view;

import controllers.JanelaCardapioClickListener;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import model.pedido.Produto;
import model.usuarios.Funcionario;

public class JanelaCardapio extends View{
    private Produto produto;
    private Funcionario funcionario;
    private Date data;
    private JPanel panel;
    private JLabel informacoesLabel, imagemLabel, usuarioLabel, dataLabel;
    private JButton voltarButton, adicionarItemButton, removerItemButton;
    private JScrollPane tabelaScroll;
    private JTable tabela;
    private DefaultTableModel modelo;
    private GridBagConstraints restricoes;
    
    public JanelaCardapio(JFrame mainFrame, Funcionario funcionario){
        this.funcionario = funcionario;
        setMainFrame(mainFrame);
        preparaGUI();
    }
    
    public void preparaGUI(){
        panel = new JPanel(new GridBagLayout());
        restricoes = new GridBagConstraints();
        imagemLabel = new JLabel(new ImageIcon("default_files/imagem_padrao2.png"));
        usuarioLabel = new JLabel("Funcionário: " + funcionario.getNome());
        dataLabel = new JLabel("Data: " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
        informacoesLabel = new JLabel("Modifique o cardápio clicando 2 vezes em cada cédula ou clique no botão 'Adicionar item'");
        voltarButton = new JButton("Voltar");
        adicionarItemButton = new JButton("Adicionar Item");
        removerItemButton = new JButton("Remover item selecionado");
        
        String[] nomeColunas = {"N° produto", "Nome", "Categoria", "Qtd. estoque", "Qtd. mínima", "Preço"};
        modelo = new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            nomeColunas
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class,java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, true, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        };
        tabela = new JTable(modelo);
        for(int i = 0; i<6; i++){
            TableColumn coluna = new TableColumn();
            coluna = tabela.getColumnModel().getColumn(i);
            if(i == 0){
                coluna.setPreferredWidth(50);
            } else if(i == 1 || i==2){
                coluna.setPreferredWidth(120);
            } else if(i==3 || i==4){
                coluna.setPreferredWidth(80);
            }
        }
        tabelaScroll = new JScrollPane(tabela);
        tabelaScroll.setPreferredSize(new Dimension(610, 420));
        tabela.setFillsViewportHeight(true);
        
        File cardapios = new File("default_files/cardapios.txt");
        produto = new Produto();
        try{
            if(cardapios.exists()){
                int numeroLinhas = 1, linhas = 0;
                Scanner leitor = new Scanner(cardapios);
                modelo.setNumRows(numeroLinhas);
                while(leitor.hasNextLine()){
                    modelo.setNumRows(numeroLinhas);
                    String linha = leitor.nextLine();
                    produto.setIdProduto(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setNomeProduto(linha);
                    linha = leitor.nextLine();
                    produto.setCategoria(linha);
                    linha = leitor.nextLine();
                    produto.setQuantidade(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setQuantidadeMinima(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setPrecoUnidade(Double.parseDouble(linha));
                    for(int coluna = 0; coluna < 6; coluna++){
                        switch (coluna) {
                            case 0:
                                modelo.setValueAt(produto.getIdProduto(), linhas, coluna);
                                break;
                            case 1:
                                modelo.setValueAt(produto.getNomeProduto(), linhas, coluna);
                                break;
                            case 2:
                                modelo.setValueAt(produto.getCategoria(), linhas, coluna);
                                break;
                            case 3:
                                modelo.setValueAt(produto.getQuantidade(), linhas, coluna);
                                break;
                            case 4:
                                modelo.setValueAt(produto.getQuantidadeMinima(), linhas, coluna);
                                break;
                            case 5:
                                modelo.setValueAt(produto.getPrecoUnidade(), linhas, coluna);
                                break;
                            default:
                                break;
                        }
                    }
                    linhas++;
                    numeroLinhas++;
                }
            } else{
                throw new FileNotFoundException();
            }
        } catch(FileNotFoundException ex){
            JOptionPane.showMessageDialog(null, "Arquivo 'cardapios.txt' não encontrado na pasta padrão 'default_files'.", "Erro", JOptionPane.ERROR_MESSAGE);
        }
        
        restricoes.gridx = 0;
        restricoes.gridy = 0;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 11;
        restricoes.insets = new java.awt.Insets(10, 0, 10, 10);
        panel.add(imagemLabel, restricoes);
        
        restricoes.gridx = 13;
        restricoes.gridy = 1;
        restricoes.gridwidth = 3;
        restricoes.gridheight = 2;
        restricoes.insets = new java.awt.Insets(10, 10, 10, 10);
        panel.add(usuarioLabel, restricoes);
        
        restricoes.gridx = 13;
        restricoes.gridy = 3;
        restricoes.gridwidth = 3;
        restricoes.gridheight = 2;
        restricoes.anchor = GridBagConstraints.WEST;
        restricoes.insets = new java.awt.Insets(10, 10, 10, 10);
        panel.add(dataLabel, restricoes);
        
        restricoes.gridx = 14;
        restricoes.gridy = 10;
        restricoes.gridwidth = 10;
        restricoes.gridheight = 1;
        restricoes.anchor = GridBagConstraints.CENTER;
        restricoes.insets = new java.awt.Insets(10, 20, 5, 10);
        panel.add(informacoesLabel, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 12;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 1;
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.insets = new java.awt.Insets(24, 0, 10, 0);
        panel.add(adicionarItemButton, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 13;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 1;
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.insets = new java.awt.Insets(24, 0, 10, 0);
        panel.add(removerItemButton, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 14;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 1;
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.insets = new java.awt.Insets(24, 0, 10, 0);
        panel.add(voltarButton, restricoes);
        
        restricoes.gridx = 14;
        restricoes.gridy = 11;
        restricoes.gridwidth = 24;
        restricoes.gridheight = 14;
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.insets = new Insets(0, 24, 0, 0);
        panel.add(tabelaScroll, restricoes);
        
        getMainFrame().add(panel);
        getMainFrame().setVisible(true);
        
        controles();
    }
    public void controles(){
        voltarButton.setActionCommand("voltar");
        adicionarItemButton.setActionCommand("adicionar");
        removerItemButton.setActionCommand("remover");
        
        voltarButton.addActionListener(new JanelaCardapioClickListener(getMainFrame(), panel, funcionario, tabela));
        adicionarItemButton.addActionListener(new JanelaCardapioClickListener(modelo));
        removerItemButton.addActionListener(new JanelaCardapioClickListener(tabela, modelo));
    }
}