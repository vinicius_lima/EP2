package view;

import controllers.JanelaVendaClickListener;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.pedido.Pedido;
import model.pedido.Produto;
import model.usuarios.Funcionario;


public class JanelaVenda extends View {
    private JComboBox itens, mesas, formaPagamento;
    private Produto produto;
    private Funcionario funcionario;
    private Pedido pedido;
    private JLabel nomeItem, nomeCliente, quantidade, imagemLabel, mesaLabel, pagamentoLabel, valorFinalLabel, observacoesClienteLabel;
    private JPanel mainPanel, panel1, panel2, panel3, panelObservacoesCliente;
    private JFormattedTextField quantidadeText, clienteText, observacoesClienteText;
    private JTable tabelaProduto, tabelaCart;
    private DefaultTableModel modeloTableProduto, modeloTabelCart;
    private JScrollPane tabelaProdutoPanel, tabelaCartPanel;
    private JButton addCartButton, finalizaPedidoButton, removerItemButton, voltaButton;
    private GridBagConstraints restricoes;
    private NumberFormat formatoQuantidadeText;

    public JanelaVenda(Funcionario funcionario, JFrame mainFrame) throws ParseException {
        setMainFrame(mainFrame);
        this.funcionario = funcionario;
        preparaGUI();
    }
    
    public void preparaGUI() throws ParseException{
        mainPanel = new JPanel(new GridBagLayout());
        panel1 = new JPanel(new GridBagLayout());
        panel2 = new JPanel();
        panel3 = new JPanel(new GridLayout(2, 1));
        nomeItem = new JLabel("Produto:");
        nomeCliente = new JLabel("Cliente:");
        quantidade = new JLabel("Quantidade:");
        valorFinalLabel = new JLabel("Valor final: " + "R$ 0.00");
        imagemLabel = new JLabel(new ImageIcon("default_files/imagem_padrao2.png"));
        mesaLabel = new JLabel("Mesa:");
        pagamentoLabel = new JLabel("Pagamento:");
        observacoesClienteLabel = new JLabel("Observações do Cliente: ");
        observacoesClienteText = new JFormattedTextField();
        panelObservacoesCliente = new JPanel(new FlowLayout());
        formatoQuantidadeText = NumberFormat.getIntegerInstance();
        formatoQuantidadeText.setMaximumFractionDigits(0);
        quantidadeText = new JFormattedTextField(formatoQuantidadeText);
        quantidadeText.setText(String.valueOf(0));
        clienteText = new JFormattedTextField();
        addCartButton = new JButton("Adicionar no carrinho");
        finalizaPedidoButton = new JButton("Finalizar pedido");
        removerItemButton = new JButton("Remover Item do Carrinho");
        voltaButton = new JButton("Voltar");
        restricoes = new GridBagConstraints();
        produto = new Produto();
        itens = new JComboBox();
        mesas = new JComboBox();
        formaPagamento = new JComboBox();
        
        
        for(int i=0; i<=20; i++){
            if(i==0){
                mesas.addItem("");
            } else{ 
                mesas.addItem(i);
            }
        }
        
        formaPagamento.addItem("");
        formaPagamento.addItem("Dinheiro");
        formaPagamento.addItem("Cartão de Crédito");
        
        modeloTableProduto = new DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "N° produto", "Nome", "Qtd. estoque", "Qtd. mínima", "Preço"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        };
        
        modeloTabelCart = new DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Nome item", "Quantia", "Valor"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        };
        
        File cardapios = new File("default_files/cardapios.txt");
        try{
            itens.addItem("");
            if(cardapios.exists()){
                Scanner leitor = new Scanner(cardapios);
                while(leitor.hasNextLine()){
                    String linha = leitor.nextLine();
                    produto.setIdProduto(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setNomeProduto(linha);
                    linha = leitor.nextLine();
                    produto.setCategoria(linha);
                    linha = leitor.nextLine();
                    produto.setQuantidade(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setQuantidadeMinima(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setPrecoUnidade(Double.parseDouble(linha));
                    itens.addItem(produto.getNomeProduto());
                }
                leitor.close();
            } else{
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException ex){
            JOptionPane.showMessageDialog(null, "Arquivo 'cardapios.txt' não encontrado na pasta padrão 'default_files'.", "Erro", JOptionPane.ERROR_MESSAGE);
        }
        
        itens.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                try{
                    modeloTableProduto.setNumRows(1);
                    if(cardapios.exists()){
                        Scanner leitor = new Scanner(cardapios);
                        while(leitor.hasNextLine()){
                            String linha = leitor.nextLine();
                            produto.setIdProduto(Integer.parseInt(linha));
                            linha = leitor.nextLine();
                            produto.setNomeProduto(linha);
                            linha = leitor.nextLine();
                            produto.setCategoria(linha);
                            linha = leitor.nextLine();
                            produto.setQuantidade(Integer.parseInt(linha));
                            linha = leitor.nextLine();
                            produto.setQuantidadeMinima(Integer.parseInt(linha));
                            linha = leitor.nextLine();
                            produto.setPrecoUnidade(Double.parseDouble(linha));
                            if(itens.getSelectedItem().equals(produto.getNomeProduto())){
                                modeloTableProduto.setValueAt(produto.getIdProduto(), 0, 0);
                                modeloTableProduto.setValueAt(produto.getNomeProduto(), 0, 1);
                                modeloTableProduto.setValueAt(produto.getQuantidade(), 0, 2);
                                modeloTableProduto.setValueAt(produto.getQuantidadeMinima(), 0, 3);
                                modeloTableProduto.setValueAt(produto.getPrecoUnidade(), 0, 4);
                            }
                        }
                        leitor.close();
                    } else{
                        throw new FileNotFoundException();
                    }
                } catch (FileNotFoundException ex){
                    JOptionPane.showMessageDialog(null, "Arquivo 'cardapios.txt' não encontrado na pasta padrão 'default_files'.", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        tabelaProduto = new JTable(modeloTableProduto);
        tabelaProdutoPanel = new JScrollPane(tabelaProduto);
        tabelaProdutoPanel.setPreferredSize(new Dimension(600, 200));
        
        modeloTabelCart.setNumRows(0);
        tabelaCart = new JTable(modeloTabelCart);
        tabelaCartPanel = new JScrollPane(tabelaCart);
        tabelaCartPanel.setPreferredSize(new Dimension(600, 200));
        
        restricoes.gridx = 0;
        restricoes.gridy = 0;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 12;
        restricoes.insets = new Insets(10, 10, 10, 10);
        restricoes.fill = GridBagConstraints.NONE;
        panel1.add(imagemLabel, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 12;
        restricoes.gridwidth = 3;
        restricoes.gridheight = 2;
        restricoes.insets = new Insets(30, 5, 10, 10);
        restricoes.anchor = GridBagConstraints.WEST;
        panel1.add(nomeItem, restricoes);
        
        restricoes.gridx = 3;
        restricoes.gridy = 12;
        restricoes.gridwidth = 9;
        restricoes.gridheight = 2;
        restricoes.insets = new Insets(20, 5, 10, 10);
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.anchor = GridBagConstraints.CENTER;
        panel1.add(itens, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 14;
        restricoes.gridwidth = 3;
        restricoes.gridheight = 2;
        restricoes.insets = new Insets(10, 5, 10, 10);
        restricoes.anchor = GridBagConstraints.WEST;
        panel1.add(quantidade, restricoes);
        
        restricoes.gridx = 3;
        restricoes.gridy = 14;
        restricoes.gridwidth = 9;
        restricoes.gridheight = 2;
        restricoes.insets = new Insets(10, 5, 10, 10);
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.anchor = GridBagConstraints.CENTER;
        panel1.add(quantidadeText, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 16;
        restricoes.gridwidth = 3;
        restricoes.gridheight = 2;
        restricoes.insets = new Insets(10, 5, 10, 10);
        restricoes.anchor = GridBagConstraints.WEST;
        panel1.add(nomeCliente, restricoes);
        
        restricoes.gridx = 3;
        restricoes.gridy = 16;
        restricoes.gridwidth = 9;
        restricoes.gridheight = 2;
        restricoes.insets = new Insets(10, 5, 10, 10);
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.anchor = GridBagConstraints.CENTER;
        panel1.add(clienteText, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 18;
        restricoes.gridwidth = 3;
        restricoes.gridheight = 2;
        restricoes.insets = new Insets(10, 5, 10, 10);
        restricoes.anchor = GridBagConstraints.WEST;
        panel1.add(mesaLabel, restricoes);
        
        restricoes.gridx = 3;
        restricoes.gridy = 18;
        restricoes.gridwidth = 9;
        restricoes.gridheight = 2;
        restricoes.insets = new Insets(10, 5, 10, 10);
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.anchor = GridBagConstraints.CENTER;
        panel1.add(mesas, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 20;
        restricoes.gridwidth = 4;
        restricoes.gridheight = 2;
        restricoes.insets = new Insets(10, 5, 10, 10);
        restricoes.anchor = GridBagConstraints.WEST;
        panel1.add(pagamentoLabel, restricoes);
        
        restricoes.gridx = 3;
        restricoes.gridy = 20;
        restricoes.gridwidth = 8;
        restricoes.gridheight = 2;
        restricoes.insets = new Insets(10, 5, 10, 10);
        restricoes.fill = GridBagConstraints.HORIZONTAL;
        restricoes.anchor = GridBagConstraints.EAST;
        panel1.add(formaPagamento, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 22;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 1;
        restricoes.insets = new Insets(20, 5, 10, 10);
        restricoes.anchor = GridBagConstraints.CENTER;
        panel1.add(addCartButton, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 23;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 1;
        restricoes.insets = new Insets(10, 5, 10, 10);
        restricoes.anchor = GridBagConstraints.CENTER;
        panel1.add(removerItemButton, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 24;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 7;
        restricoes.insets = new Insets(10, 5, 10, 10);
        restricoes.anchor = GridBagConstraints.CENTER;
        panel1.add(finalizaPedidoButton, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 32;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 1;
        restricoes.insets = new Insets(10, 5, 5, 10);
        restricoes.anchor = GridBagConstraints.CENTER;
        panel1.add(voltaButton, restricoes);
        
        restricoes.gridx = 0;
        restricoes.gridy = 0;
        restricoes.gridwidth = 12;
        restricoes.gridheight = 31;
        restricoes.insets = new Insets(0, 0, 0, 0);
        mainPanel.add(panel1, restricoes);
        panel1.setVisible(true);
        
        panel2.add(tabelaProdutoPanel);
        
        restricoes.gridx = 12;
        restricoes.gridy = 0;
        restricoes.gridwidth= 30;
        restricoes.gridheight = 15;
        restricoes.insets = new Insets(10, 20, 30, 10);
        mainPanel.add(panel2, restricoes);
        
        panel2.setVisible(true);
        
        panelObservacoesCliente.add(observacoesClienteLabel);
        panelObservacoesCliente.add(observacoesClienteText);
        
        panel3.add(tabelaCartPanel);
        panel3.add(valorFinalLabel);
        
        observacoesClienteText.setColumns(30);
        
        restricoes.gridx = 12;
        restricoes.gridy = 15;
        restricoes.gridwidth = 30;
        restricoes.gridheight = 15;
        restricoes.insets = new Insets(10, 20, 0, 10);
        mainPanel.add(panel3, restricoes);
        panel3.setVisible(true);
        
        restricoes.gridx = 12;
        restricoes.gridy = 31;
        restricoes.gridwidth = 6;
        restricoes.gridheight = 1;
        restricoes.insets = new Insets(0, 20, 0, 0);
        mainPanel.add(panelObservacoesCliente, restricoes);
        panel3.setVisible(true);
        
        getMainFrame().add(mainPanel);
        getMainFrame().setVisible(true);
        
        controles();
    }
    public void controles(){
        voltaButton.setActionCommand("voltar");
        addCartButton.setActionCommand("adicionar");
        finalizaPedidoButton.setActionCommand("finalizar");
        removerItemButton.setActionCommand("remover");
        
        voltaButton.addActionListener(new JanelaVendaClickListener(getMainFrame(), funcionario, mainPanel));
        addCartButton.addActionListener(new JanelaVendaClickListener(modeloTabelCart, itens, quantidadeText, valorFinalLabel));
        removerItemButton.addActionListener(new JanelaVendaClickListener(modeloTabelCart, tabelaCart, valorFinalLabel));
        finalizaPedidoButton.addActionListener(new JanelaVendaClickListener(funcionario, modeloTabelCart, formaPagamento, mesas, clienteText, tabelaCart, valorFinalLabel, modeloTableProduto, itens, observacoesClienteText));
    }
}