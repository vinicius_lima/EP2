package view;

import controllers.JanelaPrincipalClickListener;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import model.pedido.Produto;
import model.usuarios.Funcionario;

public class JanelaPrincipal extends View{
    private Date data;
    private Funcionario funcionario;
    private Produto produto;
    private JPanel mainPanel;
    private JLabel imagemLabel, usuarioLabel, dataLabel, cardapioLabel;
    private JButton sairButton, vendaButton, cardapioButton, trocarSenhaButton, relatorioButton;
    private JScrollPane tabelaPanel;
    private JTable tabelaCardapio;
    private DefaultTableModel modelo;
    
    public JanelaPrincipal(JFrame mainFrame, Funcionario funcionario) {
        this.funcionario = funcionario;
        setMainFrame(mainFrame);
        preparaGUI();
    }
    
    public JanelaPrincipal(){
        preparaGUI();
    }
    
    public void preparaGUI(){
        getMainFrame().setSize(1000, 700);
        getMainFrame().setLocation(183, 34);
        
        getMainFrame().addWindowListener(new WindowAdapter(){
                @Override
                public void windowClosing(WindowEvent windowEvent){
                    System.exit(0);
                }
        });
        
        data = new Date();
        GridBagConstraints gbc = new GridBagConstraints();
        mainPanel = new JPanel(new GridBagLayout());
        
        imagemLabel = new JLabel();
        imagemLabel.setIcon(new ImageIcon("default_files/imagem_padrao2.png"));
        usuarioLabel = new JLabel("Funcionário: " + funcionario.getNome());
        dataLabel = new JLabel("Data: " + new SimpleDateFormat("dd/MM/yyyy").format(data));
        cardapioLabel = new JLabel("Refeições:");
                
        sairButton = new JButton("Sair");
        vendaButton = new JButton("Nova venda");
        cardapioButton = new JButton("Atualizar cardápio");
        trocarSenhaButton = new JButton("Trocar senha");
        relatorioButton = new JButton("Relatório de vendas");
        produto = new Produto();

        String[] nomeColunas = {"N° produto", "Nome", "Categoria", "Quantidade", "Preço"};
        
        modelo = new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            nomeColunas
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        };
        tabelaCardapio = new JTable(modelo);
        for(int i = 0; i<5; i++){
            TableColumn coluna = new TableColumn();
            coluna = tabelaCardapio.getColumnModel().getColumn(i);
            if(i == 0){
                coluna.setPreferredWidth(50);
            } else if(i == 1 || i==2){
                coluna.setPreferredWidth(120);
            } else if(i==3 || i==4){
                coluna.setPreferredWidth(80);
            }
        }
        tabelaPanel = new JScrollPane(tabelaCardapio);
        tabelaPanel.setPreferredSize(new Dimension(590, 420));
        tabelaCardapio.setFillsViewportHeight(true);
                
        File cardapios = new File("default_files/cardapios.txt");
        try{
            if(cardapios.exists()){
                int numeroLinhas = 1, linhas = 0;
                Scanner leitor = new Scanner(cardapios);
                modelo.setNumRows(numeroLinhas);
                while(leitor.hasNextLine()){
                    modelo.setNumRows(numeroLinhas);
                    String linha = leitor.nextLine();
                    produto.setIdProduto(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setNomeProduto(linha);
                    linha = leitor.nextLine();
                    produto.setCategoria(linha);
                    linha = leitor.nextLine();
                    produto.setQuantidade(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setQuantidadeMinima(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setPrecoUnidade(Double.parseDouble(linha));
                    for(int coluna = 0; coluna < 5; coluna++){
                        switch (coluna) {
                            case 0:
                                modelo.setValueAt(produto.getIdProduto(), linhas, coluna);
                                break;
                            case 1:
                                modelo.setValueAt(produto.getNomeProduto(), linhas, coluna);
                                break;
                            case 2:
                                modelo.setValueAt(produto.getCategoria(), linhas, coluna);
                                break;
                            case 3:
                                modelo.setValueAt(produto.getQuantidade(), linhas, coluna);
                                break;
                            case 4:
                                modelo.setValueAt(produto.getPrecoUnidade(), linhas, coluna);
                                break;
                            default:
                                break;
                        }
                    }
                    linhas++;
                    numeroLinhas++;
                }
                leitor.close();
            } else{
                throw new FileNotFoundException();
            }
        } catch(FileNotFoundException ex){
            JOptionPane.showMessageDialog(null, "Arquivo 'cardapios.txt' não encontrado na pasta padrão 'default_files'.", "Erro", JOptionPane.ERROR_MESSAGE);
        }
      
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 12;
        gbc.gridheight = 11;
        gbc.insets = new java.awt.Insets(10, 10, 10, 10);
        mainPanel.add(imagemLabel, gbc);
        
        gbc.gridx = 12;
        gbc.gridy = 0;
        gbc.gridwidth = 45;
        gbc.gridheight = 1;
        gbc.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gbc.insets = new java.awt.Insets(10, 10, 10, 10);
        mainPanel.add(usuarioLabel, gbc);
        
        gbc.gridx = 12;
        gbc.gridy = 1;
        gbc.gridwidth = 3;
        gbc.gridheight = 2;
        gbc.anchor = java.awt.GridBagConstraints.WEST;
        gbc.insets = new java.awt.Insets(10, 10, 10, 0);
        mainPanel.add(dataLabel, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 12;
        gbc.gridwidth = 12;
        gbc.gridheight = 1;
        gbc.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gbc.insets = new java.awt.Insets(24, 0, 10, 0);
        mainPanel.add(cardapioButton, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 13;
        gbc.gridwidth = 12;
        gbc.gridheight = 1;
        gbc.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gbc.insets = new java.awt.Insets(10, 0, 10, 0);
        mainPanel.add(relatorioButton, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 14;
        gbc.gridwidth = 12;
        gbc.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gbc.insets = new java.awt.Insets(10, 0, 10, 0);
        mainPanel.add(vendaButton, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 15;
        gbc.gridwidth = 12;
        gbc.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gbc.insets = new java.awt.Insets(10, 0, 30, 0);
        mainPanel.add(trocarSenhaButton, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 16;
        gbc.gridwidth = 12;
        gbc.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gbc.insets = new java.awt.Insets(30, 0, 0, 0);
        mainPanel.add(sairButton, gbc);
        
        gbc.gridx = 14;
        gbc.gridy = 10;
        gbc.gridwidth = 3;
        gbc.gridheight = 1;
        gbc.insets = new java.awt.Insets(10, 20, 10, 0);
        mainPanel.add(cardapioLabel, gbc);
        
        gbc.gridx = 14;
        gbc.gridy = 11;
        gbc.gridwidth = 24;
        gbc.gridheight = 14;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 24, 0, 0);
        mainPanel.add(tabelaPanel, gbc);
        
        getMainFrame().add(mainPanel);
        controles();
    }
    public void controles(){
        vendaButton.setActionCommand("vender");
        cardapioButton.setActionCommand("cardapio");
        relatorioButton.setActionCommand("relatorio");
        trocarSenhaButton.setActionCommand("trocar senha");
        sairButton.setActionCommand("sair");
        
        vendaButton.addActionListener(new JanelaPrincipalClickListener(getMainFrame(), mainPanel, funcionario));
        cardapioButton.addActionListener(new JanelaPrincipalClickListener(getMainFrame(), mainPanel, funcionario));
        relatorioButton.addActionListener(new JanelaPrincipalClickListener(getMainFrame(), mainPanel, funcionario));
        trocarSenhaButton.addActionListener(new JanelaPrincipalClickListener(funcionario));
        sairButton.addActionListener(new JanelaPrincipalClickListener(getMainFrame()));
    }
}