package view;

import controllers.JanelaLoginClickListener;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import model.usuarios.Funcionario;


public class JanelaLogin extends View {
    private JLabel headerLabel;
    private JLabel loginLabel;
    private JLabel senhaLabel;
    private JLabel imagemLabel;
    private JPanel mainPanel, controlPanel, backgroundPanel1, backgroundPanel2, backgroundPanel3;
    private JFormattedTextField loginText;
    private JPasswordField senhaText;
    private JButton entrarButton;
    private JButton cadastrarButton;
    private Funcionario funcionario;
    
    public JanelaLogin(){
        preparaGUI();
    }
    public void preparaGUI(){
        setMainFrame(new JFrame("Restaurante Tô com fome, quero mais"));
        getMainFrame().setSize(800, 600);
        getMainFrame().setLocation(293, 70);
       
        getMainFrame().addWindowListener(new WindowAdapter(){
                @Override
                public void windowClosing(WindowEvent windowEvent){
                    System.exit(0);
                }
        });
        
        headerLabel = new JLabel("Bem Vindo! Entre com o seu login cadastrado", (int) JLabel.CENTER_ALIGNMENT);
        headerLabel.setSize(400, 200);
        imagemLabel = new JLabel();
        imagemLabel.setHorizontalAlignment(JLabel.CENTER);
        imagemLabel.setIcon(new ImageIcon("default_files/imagem_padrao.jpg"));
        
        controlPanel = new JPanel(new GridLayout(3, 1, 2, 3));
        mainPanel = new JPanel(new GridLayout(3, 1));
        mainPanel.add(headerLabel);
        mainPanel.add(controlPanel);
        mainPanel.add(imagemLabel);
        
        criaPainel();
      
        getMainFrame().add(mainPanel);
        getMainFrame().setVisible(true);
        
        controls();
    }
    public void criaPainel(){
        backgroundPanel1 = new JPanel(new FlowLayout());
        backgroundPanel2 = new JPanel(new FlowLayout());
        backgroundPanel3 = new JPanel(new FlowLayout());
                
        loginLabel = new JLabel("Id Login: ", JLabel.CENTER);
        senhaLabel = new JLabel("Senha: ", JLabel.CENTER);
        
        loginText = new JFormattedTextField();
        loginText.setColumns(19);
        backgroundPanel1.add(loginLabel);
        backgroundPanel1.add(loginText);
        
        senhaText = new JPasswordField(20);
        senhaText.setSize(200, 100);
        backgroundPanel2.add(senhaLabel);
        backgroundPanel2.add(senhaText);
        
        entrarButton = new JButton("Entrar");
        cadastrarButton = new JButton("Cadastrar");
        
        entrarButton.setActionCommand("entrar");
        cadastrarButton.setActionCommand("cadastrar");
        
        backgroundPanel3.add(entrarButton);
        backgroundPanel3.add(cadastrarButton);
        
        controlPanel.add(backgroundPanel1);
        controlPanel.add(backgroundPanel2);
        controlPanel.add(backgroundPanel3);   
    }
    public void controls(){
        entrarButton.addActionListener(new JanelaLoginClickListener(loginText, senhaText, mainPanel, getMainFrame()));
        cadastrarButton.addActionListener(new JanelaLoginClickListener(loginText, senhaText, mainPanel, getMainFrame()));
    }
}