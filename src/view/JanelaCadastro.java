package view;

import controllers.JanelaCadastroClickListener;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import model.usuarios.Funcionario;


public class JanelaCadastro extends View{
    private JPanel painel;
    private JPanel painelSecundario1;
    private JPanel painelSecundario2;
    private JPanel painelSecundario3;
    private JPanel painelSecundario4;
    private JPanel painelSecundario5;
    private JLabel nomeLabel;
    private JLabel idLabel;
    private JLabel senhaLabel;
    private JLabel confirmaSenhaLabel;
    private JFormattedTextField idText;
    private JFormattedTextField nomeText;
    private JPasswordField senhaText;
    private JPasswordField confirmaSenhaText;
    private JButton cancelarButton;
    private JButton okButton;
    
    public JanelaCadastro(){
        preparaGUI();
    }
    
    public void preparaGUI(){
        setMainFrame(new JFrame("Cadastro"));
        painel = new JPanel(new GridLayout(5, 1));
        painelSecundario1 = new JPanel(new FlowLayout());
        painelSecundario2 = new JPanel(new FlowLayout());
        painelSecundario3 = new JPanel(new FlowLayout());
        painelSecundario4 = new JPanel(new FlowLayout());
        painelSecundario5 = new JPanel(new FlowLayout());
        nomeLabel = new JLabel("Nome:                ");
        idLabel = new JLabel("Id Login:             ");
        senhaLabel = new JLabel("Senha:                ");
        confirmaSenhaLabel = new JLabel("Confirme senha:");
        idText = new JFormattedTextField();
        nomeText = new JFormattedTextField();
        senhaText = new JPasswordField();
        confirmaSenhaText = new JPasswordField();
        cancelarButton = new JButton("Cancelar");
        okButton = new JButton("Ok");

        idText.setColumns(20);
        nomeText.setColumns(20);
        senhaText.setColumns(20);
        confirmaSenhaText.setColumns(20);

        painelSecundario1.add(nomeLabel);
        painelSecundario1.add(nomeText);
        painelSecundario2.add(idLabel);
        painelSecundario2.add(idText);
        painelSecundario3.add(senhaLabel);
        painelSecundario3.add(senhaText);
        painelSecundario4.add(confirmaSenhaLabel);
        painelSecundario4.add(confirmaSenhaText);
        painelSecundario5.add(okButton);
        painelSecundario5.add(cancelarButton);

        painel.add(painelSecundario1);
        painel.add(painelSecundario2);
        painel.add(painelSecundario3);
        painel.add(painelSecundario4);
        painel.add(painelSecundario5);

        getMainFrame().add(painel);

        getMainFrame().pack();
        getMainFrame().setLocation(300, 200);
        getMainFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        getMainFrame().setVisible(true);
        controles();
    }
    public void controles(){
        okButton.setActionCommand("OK");
        cancelarButton.setActionCommand("Cancelar");
        okButton.addActionListener(new JanelaCadastroClickListener(nomeText, idText, senhaText, confirmaSenhaText, getMainFrame()));
        cancelarButton.addActionListener(new JanelaCadastroClickListener(nomeText, idText, senhaText, confirmaSenhaText, getMainFrame()));
    }
}
