package controllers;
import model.usuarios.Funcionario;
import view.JanelaCadastro;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import view.JanelaPrincipal;

public class JanelaLoginClickListener implements ActionListener {
    private Funcionario funcionario;
    private JFormattedTextField loginText;
    private JPasswordField senhaText;
    private JPanel mainPanel;
    private JFrame mainFrame;

    public JanelaLoginClickListener(JFormattedTextField loginText, JPasswordField senhaText, JPanel mainPanel, JFrame mainFrame){
        this.loginText = loginText;
        this.senhaText = senhaText;
        this.mainPanel = mainPanel;
        this.mainFrame = mainFrame;
    }
    @Override
    public void actionPerformed(ActionEvent e){
        String comando = e.getActionCommand();
        funcionario = new Funcionario();
        if(comando.equals("entrar")){
            
            funcionario.setLogin(loginText.getText());
            funcionario.setSenha(senhaText.getText());
            
            File cadastrados = new File("default_files/cadastros.txt");
            String nome;
            String loginId; 
            String senha;
            try{
                Scanner reader = new Scanner(cadastrados);
                boolean usuarioValido = false;
                while(reader.hasNextLine()){
                    nome = reader.nextLine();
                    loginId = reader.nextLine();
                    senha = reader.nextLine();
                    if(funcionario.getLogin().equals(loginId) && funcionario.getSenha().equals(senha)){
                        funcionario.setNome(nome);
                        usuarioValido = true;
                    } 
                }
                if(usuarioValido == true){
                    JOptionPane.showMessageDialog(null, "Parabéns! Dados carregados com sucesso!");
                    mainPanel.setVisible(false);
                    mainFrame.remove(mainPanel);
                    JanelaPrincipal janelaPrincipal = new JanelaPrincipal(mainFrame, funcionario);
                } else{
                    JOptionPane.showMessageDialog(null, "Usuário não encontrado. Digite novamente!", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Arquivo cadastros.txt não encontrado. Certifique-se que este arquivo encontra-se na pasta default_files", "Erro", JOptionPane.ERROR_MESSAGE);
            } finally{
                cadastrados.exists();
            }
            
        } else if(comando.equals("cadastrar")){
            JanelaCadastro janelaCadastrar = new JanelaCadastro();
        }
    }
}