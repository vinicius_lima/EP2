package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JPanel;
import model.usuarios.Funcionario;
import view.JanelaCardapio;
import view.JanelaLogin;
import view.JanelaRelatorioVendas;
import view.JanelaTrocaSenha;
import view.JanelaVenda;


public class JanelaPrincipalClickListener implements ActionListener{
    private JFormattedTextField pesquisaText;
    private JPanel mainPanel;
    private JFrame mainFrame;
    private Funcionario funcionario;

    public JanelaPrincipalClickListener() {
    }
    
    public JanelaPrincipalClickListener(Funcionario funcionario){
        this.funcionario = funcionario;
    }
    
    public JanelaPrincipalClickListener(JFrame mainFrame){
        this.mainFrame = mainFrame;
    }
    
    public JanelaPrincipalClickListener(JFrame mainFrame, JPanel mainPanel, Funcionario funcionario){
        this.funcionario = funcionario;
        this.mainFrame = mainFrame;
        this.mainPanel = mainPanel;
    }
    
    public JanelaPrincipalClickListener(JFormattedTextField pesquisaText){
        this.pesquisaText = pesquisaText;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        if(comando.equals("vender")){
            mainFrame.remove(mainPanel);
            mainPanel.setVisible(false);
            try {
                JanelaVenda venda = new JanelaVenda(funcionario, mainFrame);
            } catch (ParseException ex) {
                Logger.getLogger(JanelaPrincipalClickListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if(comando.equals("cardapio")){
            mainFrame.remove(mainPanel);
            mainPanel.setVisible(false);
            JanelaCardapio cardapios = new JanelaCardapio(mainFrame, funcionario);
        } else if(comando.equals("relatorio")){
            mainFrame.remove(mainPanel);
            mainPanel.setVisible(false);
            JanelaRelatorioVendas relatorioVendas = new JanelaRelatorioVendas(funcionario, mainFrame);
        } else if(comando.equals("trocar senha")){
            JanelaTrocaSenha trocarSenha = new JanelaTrocaSenha(funcionario);
        } else if(comando.equals("sair")){
            mainFrame.dispose();
            JanelaLogin janelaLogin = new JanelaLogin();
        } 
    }
    
}
