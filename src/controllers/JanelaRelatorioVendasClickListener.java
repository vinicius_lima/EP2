package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import model.pedido.Pedido;
import model.usuarios.Funcionario;
import view.JanelaPrincipal;

public class JanelaRelatorioVendasClickListener implements ActionListener{
    private JComboBox mesSelecionado, diaSelecionado, anoSelecionado;
    private Pedido pedido;
    private DefaultTableModel modelo;
    private JFrame mainFrame;
    private JPanel mainPanel;
    private Funcionario funcionario;

    public JanelaRelatorioVendasClickListener(JComboBox mesSelecionado, JComboBox diaSelecionado, JComboBox anoSelecionado, DefaultTableModel modelo) {
        this.mesSelecionado = mesSelecionado;
        this.diaSelecionado = diaSelecionado;
        this.anoSelecionado = anoSelecionado;
        this.modelo = modelo;
    }

    public JanelaRelatorioVendasClickListener(JFrame mainFrame, JPanel mainPanel, Funcionario funcionario) {
        this.mainFrame = mainFrame;
        this.mainPanel = mainPanel;
        this.funcionario = funcionario;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        if(comando.equals("voltar")){
            mainPanel.setVisible(false);
            mainFrame.remove(mainPanel);
            JanelaPrincipal janelaPrincipal = new JanelaPrincipal(mainFrame, funcionario);
        } else if(comando.equals("pesquisar")){
            String mes = "0";
            if(mesSelecionado.getSelectedItem().toString().equals("Janeiro")){
                mes = "01";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Fevereiro")){
                mes = "02";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Março")){
                mes = "03";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Abril")){
                mes = "04";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Maio")){
                mes = "05";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Junho")){
                mes = "06";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Julho")){
                mes = "07";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Agosto")){
                mes = "08";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Setembro")){
                mes = "09";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Outubro")){
                mes  = "10";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Novembro")){
                mes  = "11";
            } else if(mesSelecionado.getSelectedItem().toString().equals("Dezembro")){
                mes = "12";
            } else if(mesSelecionado.getSelectedItem().toString().equals("")){
                JOptionPane.showMessageDialog(null, "Selecione um mês", "Erro", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(anoSelecionado.getSelectedItem().toString().equals("")){
                JOptionPane.showMessageDialog(null, "Selecione um ano", "Erro", JOptionPane.ERROR_MESSAGE);
            }
            File vendas = new File("default_files/" + "vendas_" + diaSelecionado.getSelectedItem().toString() + '_' + mes + '_' + anoSelecionado.getSelectedItem().toString() + ".txt");
            try{
                if(vendas.exists()){
                    int numeroLinhasTabela = 1, i=0;
                    Scanner leito = new Scanner(vendas);
                    String linha, nomeCliente, mesa, nomeFuncionario, data, hora, pagamento, valorFinal, observacoes;
                    while(leito.hasNextLine()){
                        modelo.setNumRows(numeroLinhasTabela);
                        linha = leito.nextLine();
                        nomeCliente = linha;
                        linha = leito.nextLine();
                        mesa = linha;
                        linha = leito.nextLine();
                        nomeFuncionario = linha;
                        linha = leito.nextLine();
                        data = linha;
                        linha = leito.nextLine();
                        hora = linha;
                        linha = leito.nextLine();
                        pagamento = linha;
                        linha = leito.nextLine();
                        valorFinal = linha;
                        linha = leito.nextLine();
                        observacoes = linha;
                        modelo.setValueAt(Integer.parseInt(mesa), i, 0);
                        modelo.setValueAt(nomeCliente, i, 1);
                        modelo.setValueAt(nomeFuncionario, i, 2);
                        modelo.setValueAt(data, i, 3);
                        modelo.setValueAt(hora, i, 4);
                        modelo.setValueAt(pagamento, i, 5);
                        modelo.setValueAt(Double.parseDouble(valorFinal), i, 6);
                        modelo.setValueAt(observacoes, i, 7);
                        i++;
                        numeroLinhasTabela++;
                    }
                } else{
                    throw new FileNotFoundException();
                }
            } catch(FileNotFoundException ex){
                JOptionPane.showMessageDialog(null, "Não há registro de vendas nesse dia", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
