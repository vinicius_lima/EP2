package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import model.usuarios.Funcionario;


public class JanelaTrocaSenhaClickListener implements ActionListener{
    private JPasswordField senhaText;
    private JPasswordField confirmaSenhaText;
    private JFrame mainFrame;
    private Funcionario funcionario;
    
    public JanelaTrocaSenhaClickListener(JFrame mainFrame){
        this.mainFrame = mainFrame;
    }

    public JanelaTrocaSenhaClickListener(JPasswordField senhaText, JPasswordField confirmaSenhaText, Funcionario funcionario, JFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.funcionario = funcionario;
        this.senhaText = senhaText;
        this.confirmaSenhaText = confirmaSenhaText;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        if(comando.equals("OK")){
            File cadastros = new File("default_files/cadastros.txt");
            try{
                if(cadastros.exists()){
                    String senha = senhaText.getText();
                    String confirmaSenha = confirmaSenhaText.getText();
                    if(senha.equals(confirmaSenha) && !senha.equals("") && !confirmaSenha.equals("")){
                        List<String> linhas = new ArrayList<>(Files.readAllLines(cadastros.toPath(), StandardCharsets.UTF_8));
                        for(int i=0; i<linhas.size(); i=i+3){
                            if(linhas.get(i).equals(funcionario.getNome()) && linhas.get(i+1).equals(funcionario.getLogin()) && linhas.get(i+2).equals(funcionario.getSenha())){
                                linhas.set(i+2, senha);
                                break;
                            }
                        }
                        Files.write(cadastros.toPath(), linhas, StandardCharsets.UTF_8);
                        JOptionPane.showMessageDialog(null, "Informações alteradas");
                        mainFrame.dispose();
                    } else{
                        throw new NullPointerException();
                    }
                } else{
                    throw new IOException();
                }
            } catch(IOException ex){
                JOptionPane.showMessageDialog(null, "Arquivo 'cadastros.txt' não encontrado na pasta padrão 'default_files'.", "Erro", JOptionPane.ERROR_MESSAGE);
            } catch (NullPointerException a){
                JOptionPane.showMessageDialog(null, "Campos 'senha' e 'confirmar senha' estão diferentes!", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }else if(comando.equals("cancelar")){
                mainFrame.dispose();
        }
    }
}


