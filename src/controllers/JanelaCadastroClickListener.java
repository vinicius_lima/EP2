package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import model.usuarios.Funcionario;

public class JanelaCadastroClickListener implements ActionListener{
    private JFormattedTextField nameText;
    private JFormattedTextField idText;
    private JPasswordField senhaText;
    private JPasswordField confirmaSenhaText;
    private JFrame mainFrame;
    private Funcionario funcionario;
    
    public JanelaCadastroClickListener(JFormattedTextField nameText, JFormattedTextField idText, JPasswordField senhaText, JPasswordField confirmaSenhaText, JFrame mainFrame) {
        this.nameText = nameText;
        this.idText = idText;
        this.senhaText = senhaText;
        this.confirmaSenhaText = confirmaSenhaText;
        this.mainFrame = mainFrame;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        boolean nomeDifNull = true, idDifNull = true, senhaDifNull = true, confirmaDifNull = true, senhaEqualConfirma = true;
        String comando = e.getActionCommand();
        if(comando.equals("OK")){
            funcionario = new Funcionario(nameText.getText(), idText.getText(), senhaText.getText());
            String senhaConfirmada = confirmaSenhaText.getText();
            if(funcionario.getNome().equals("")){
                nomeDifNull = false;
            } 
            if(funcionario.getLogin().equals("")){
                idDifNull = false;
            } 
            if(funcionario.getSenha().equals("")){
                senhaDifNull = false;
            } 
            if(!funcionario.getSenha().equals(senhaConfirmada)){
                senhaEqualConfirma = false;
            }
            if(senhaConfirmada.equals("")){
                confirmaDifNull = false;
            }
            if(nomeDifNull == false){
                JOptionPane.showMessageDialog(null, "Campo 'Nome' é obrigatório!", "Erro", JOptionPane.ERROR_MESSAGE);
            } else if(idDifNull == false){
                JOptionPane.showMessageDialog(null, "Campo 'Id Login' é obrigatório!", "Erro", JOptionPane.ERROR_MESSAGE);
            } else if(senhaDifNull == false){
                JOptionPane.showMessageDialog(null, "Campo 'Senha' é obrigatório!", "Erro", JOptionPane.ERROR_MESSAGE);
            } else if(confirmaDifNull == false){
                JOptionPane.showMessageDialog(null, "Campo 'Confirmar Senha' é obrigatório!", "Erro", JOptionPane.ERROR_MESSAGE);
            } else if(senhaEqualConfirma == false){
                JOptionPane.showMessageDialog(null, "Senha do campo 'Senha' e 'Confirma senha' estão diferentes!", "Erro", JOptionPane.ERROR_MESSAGE);
            } else{
                if(nomeDifNull && idDifNull && senhaDifNull && confirmaDifNull && senhaEqualConfirma){
                    File cadastros = new File("default_files/cadastros.txt");
                    try(FileWriter escritor = new FileWriter(cadastros, true)){
                        escritor.write(funcionario.getNome() + '\n');
                        escritor.write(funcionario.getLogin() + '\n');
                        escritor.write(funcionario.getSenha() + '\n');
                        JOptionPane.showMessageDialog(null, "Informções salvas com sucesso!");
                        mainFrame.dispose();
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null, "ERRO!", "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        } else if(comando.equals("Cancelar")){
            mainFrame.dispose();
        }
    }  
}
