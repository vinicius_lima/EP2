package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.pagamento.Pagamento;
import model.pedido.Pedido;
import model.pedido.Produto;
import model.usuarios.Cliente;
import model.usuarios.Funcionario;
import view.JanelaPrincipal;


public class JanelaVendaClickListener implements ActionListener{
    private JFrame mainFrame;
    private Funcionario funcionario;
    private Cliente cliente;
    private JPanel mainPanel;
    private DefaultTableModel modeloTableCart, modeloTableProduto;
    private JTable tabelaCart;
    private JComboBox itens, formaPagamento, mesa;
    private Pedido pedido;
    private JFormattedTextField quantidadeText;
    private JFormattedTextField clienteText;
    private JFormattedTextField observacoesClienteText;
    private Produto produto;
    private Pagamento pagamento;
    private List<Produto> listaProdutos;
    private JLabel valorFinalLabel;
    
    public JanelaVendaClickListener(JFrame mainFrame, Funcionario funcionario, JPanel mainPanel) {
        this.mainFrame = mainFrame;
        this.funcionario = funcionario;
        this.mainPanel = mainPanel;
    }

    public JanelaVendaClickListener(DefaultTableModel modelo, JComboBox itens, JFormattedTextField quantidadeText, JLabel valorFinalLabel) {
        this.modeloTableCart = modelo;
        this.itens = itens;
        this.quantidadeText = quantidadeText;
        this.valorFinalLabel = valorFinalLabel;
    }

    public JanelaVendaClickListener(DefaultTableModel modelo, JTable tabela, JLabel valorFinalLabel) {
        this.modeloTableCart = modelo;
        this.tabelaCart = tabela;
        this.valorFinalLabel = valorFinalLabel;
    }

    public JanelaVendaClickListener(Funcionario funcionario, DefaultTableModel modelo, JComboBox formaPagamento, JComboBox mesa, JFormattedTextField clienteText, JTable tabelaCart, JLabel valorFinal, DefaultTableModel modeloTableProduto, JComboBox itens, JFormattedTextField observacoesCliente) {
        this.funcionario = funcionario;
        this.modeloTableCart = modelo;
        this.formaPagamento = formaPagamento;
        this.mesa = mesa;
        this.clienteText = clienteText;
        this.tabelaCart = tabelaCart;
        this.valorFinalLabel = valorFinal;
        this.modeloTableProduto = modeloTableProduto;
        this.itens = itens;
        this.observacoesClienteText =observacoesCliente;
    }
   
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        if(comando.equals("voltar")){
            mainPanel.setVisible(false);
            mainFrame.remove(mainPanel);
            JanelaPrincipal janelaPrincipal = new JanelaPrincipal(mainFrame, funcionario);
        } else if(comando.equals("adicionar")){
            produto = new Produto();
            File cardapios = new File("default_files/cardapios.txt");
            String nomeProduto = itens.getSelectedItem().toString();
            boolean itemJaAdicionado = false;
            for(int i=0; i<modeloTableCart.getRowCount(); i++){
                if(nomeProduto.equals(modeloTableCart.getValueAt(i, 0))){
                    itemJaAdicionado = true;
                    break;
                } else{
                    itemJaAdicionado = false;
                }
            }
            if(itemJaAdicionado == false){
                leArquivo();
                boolean excedeEstoque = Integer.parseInt(quantidadeText.getText()) > produto.getQuantidade();
                boolean maiorQuantidadeMinima = produto.getQuantidade() > produto.getQuantidadeMinima();
                if(!excedeEstoque && maiorQuantidadeMinima){
                    Vector rowData = null;
                    modeloTableCart.addRow(rowData);
                    int i = modeloTableCart.getRowCount() - 1;
                    modeloTableCart.setValueAt(produto.getNomeProduto(), i, 0);
                    modeloTableCart.setValueAt(quantidadeText.getText(), i, 1);
                    modeloTableCart.setValueAt((Integer.parseInt(quantidadeText.getText())*produto.getPrecoUnidade()), i, 2);
                } else{
                    if(maiorQuantidadeMinima == false){
                        JOptionPane.showMessageDialog(null, "Estoque do produto '" + produto.getNomeProduto() + "' está BAIXO", "Erro", JOptionPane.ERROR_MESSAGE);
                    } else if(excedeEstoque == true){
                        JOptionPane.showMessageDialog(null, "Quantidade de produtos pedido maior que o estoque", "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                }
                atualizaValorFinal();
            } else{
                leArquivo();
                boolean excedeEstoque = Integer.parseInt(quantidadeText.getText()) > produto.getQuantidade();
                boolean maiorQuantidadeMinima = (produto.getQuantidade() - Integer.parseInt(quantidadeText.getText())) > produto.getQuantidadeMinima();
                if(!excedeEstoque && maiorQuantidadeMinima){
                    for(int i=0; i<modeloTableCart.getRowCount(); i++){
                        if(modeloTableCart.getValueAt(i, 0).equals(nomeProduto)){
                            modeloTableCart.setValueAt(quantidadeText.getText(), i, 1);
                            modeloTableCart.setValueAt((Integer.parseInt(quantidadeText.getText())*produto.getPrecoUnidade()), i, 2);
                            break;
                        }
                    }
                } else{
                    if(excedeEstoque == true){
                        JOptionPane.showMessageDialog(null, "Quantidade de produtos pedido maior que o estoque", "Erro", JOptionPane.ERROR_MESSAGE);
                    } else if(maiorQuantidadeMinima == false){
                        JOptionPane.showMessageDialog(null, "Quantidade de produtos pedido deixa estoque abaixo da quantia mínima", "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                }
                atualizaValorFinal();
            }
        } else if(comando.equals("remover")){
            int[] selectedRows = tabelaCart.getSelectedRows();
            for(int i=0; i<selectedRows.length; i++){
                modeloTableCart.removeRow(selectedRows[i] - i);
            }
            atualizaValorFinal();
        } else if(comando.equals("finalizar")){
            File cardapios = new File("default_files/cardapios.txt");
            produto = new Produto();
            pedido = new Pedido();
            pagamento = new Pagamento();
            listaProdutos = new ArrayList<>();
            boolean clienteNulo = false, carrinhoVazio = false, mesaNaoSelecionada = false, pagamentoNaoSelecionado = false;
            String observacoes;
            if(observacoesClienteText.getText().equals("")){
                observacoes = "Sem observações";
            } else{
                observacoes = observacoesClienteText.getText();
            }
            try{
                if(clienteText.getText().equals("")){
                    clienteNulo = true;
                    throw new NullPointerException();
                } else if(modeloTableCart.getRowCount() == 0){
                    carrinhoVazio = true;
                    throw new NullPointerException();
                } else if(mesa.getSelectedItem().toString().equals("")){
                    mesaNaoSelecionada = true;
                    throw new NullPointerException();
                } else if(formaPagamento.getSelectedItem().toString().equals("")){
                    pagamentoNaoSelecionado = true;
                    throw new NullPointerException();
                }else{
                    cliente = new Cliente(clienteText.getText(), Integer.parseInt(mesa.getSelectedItem().toString()), observacoes);
                }
            } catch(NullPointerException a){
                if(clienteNulo){
                    JOptionPane.showMessageDialog(null, "Digite o nome do cliente", "Erro", JOptionPane.ERROR_MESSAGE);
                } else if(carrinhoVazio){
                    JOptionPane.showMessageDialog(null, "O carrinho está vazio. Adicione itens", "Erro", JOptionPane.ERROR_MESSAGE);
                } else if(mesaNaoSelecionada){
                    JOptionPane.showMessageDialog(null, "Selecione uma mesa", "Erro", JOptionPane.ERROR_MESSAGE);
                } else if(pagamentoNaoSelecionado){
                    JOptionPane.showMessageDialog(null, "Selecione uma forma de pagamento", "Erro", JOptionPane.ERROR_MESSAGE);
                }
                return;
            }
            
            for(int i = 0; i<modeloTableCart.getRowCount(); i++){
                try{
                    if(cardapios.exists()){
                        Scanner leitor = new Scanner(cardapios);
                        while(leitor.hasNextLine()){
                            String linha = leitor.nextLine();
                            produto.setIdProduto(Integer.parseInt(linha));
                            linha = leitor.nextLine();
                            produto.setNomeProduto(linha);
                            linha = leitor.nextLine();
                            produto.setCategoria(linha);
                            linha = leitor.nextLine();
                            produto.setQuantidade(Integer.parseInt(linha));
                            linha = leitor.nextLine();
                            produto.setQuantidadeMinima(Integer.parseInt(linha));
                            linha = leitor.nextLine();
                            produto.setPrecoUnidade(Double.parseDouble(linha));
                            if(produto.getNomeProduto().equals(modeloTableCart.getValueAt(i, 0))){
                                listaProdutos.add(produto);
                                break;
                            }
                        }
                        leitor.close();
                    } else{
                        throw new FileNotFoundException();
                    }
                } catch(FileNotFoundException ex){
                    JOptionPane.showMessageDialog(null, "Arquivo 'cardapios.txt' não encontrado na pasta padrão 'default_files'.", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
            pedido.setFuncionario(funcionario);
            pedido.setCliente(cliente);
            pedido.setFormaDePagamento(pagamento);
            pedido.setProduto(listaProdutos);
            Date data = new Date();
            pedido.setData(new SimpleDateFormat("dd/MM/yyyy").format(data));
            pedido.setHora(new SimpleDateFormat("hh:mm:ss").format(data.getTime()));
            File vendas = new File("default_files/" + "vendas_" + new SimpleDateFormat("dd_MM_yyyy").format(data) + ".txt");
            try(FileWriter escritor = new FileWriter(vendas, true)){
                escritor.write(pedido.getCliente().getNome() + '\n');
                escritor.write(String.valueOf(cliente.getMesa()) + '\n');
                escritor.write(pedido.getFuncionario().getNome() + '\n');
                escritor.write(pedido.getData() + '\n');
                escritor.write(pedido.getHora() + '\n');
                if(formaPagamento.getSelectedItem().toString().equals(Pagamento.PAGAMENTO_A_VISTA)){
                    escritor.write(formaPagamento.getSelectedItem().toString() + '\n');
                } else if(formaPagamento.getSelectedItem().toString().equals(Pagamento.PAGAMENTO_CARTAO)){
                    escritor.write(formaPagamento.getSelectedItem().toString() + '\n');
                }
                Double valorFinal = 0.0;
                for(int i=0; i<modeloTableCart.getRowCount(); i++){
                    valorFinal = valorFinal + Double.parseDouble(modeloTableCart.getValueAt(i, 2).toString());
                    pagamento.setValor(valorFinal);
                }
                escritor.write(pagamento.getValor().toString() + '\n');
                escritor.write(cliente.getObservacoes() + '\n');
                atualizaCardapio();
                atualizaTela();
            } catch(IOException ex){
                
            }
        }
    }
    public void leArquivo(){
        File cardapios = new File("default_files/cardapios.txt");
        try{
            if(cardapios.exists()){
                Scanner leitor = new Scanner(cardapios);
                while(leitor.hasNextLine()){
                    String linha = leitor.nextLine();
                    produto.setIdProduto(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setNomeProduto(linha);
                    linha = leitor.nextLine();
                    produto.setCategoria(linha);
                    linha = leitor.nextLine();
                    produto.setQuantidade(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setQuantidadeMinima(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produto.setPrecoUnidade(Double.parseDouble(linha));
                    if(produto.getNomeProduto().equals(itens.getSelectedItem().toString())){
                        leitor.close();
                        break;
                    }
                }
            } else {
                throw new FileNotFoundException();
            }
        } catch(FileNotFoundException ex){
            JOptionPane.showMessageDialog(null, "Arquivo 'cardapios.txt' não encontrado na pasta padrão 'default_files'.", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void atualizaValorFinal(){
        Double valorFinal = 0.0;
        for(int i=0; i<modeloTableCart.getRowCount(); i++){
            valorFinal = valorFinal + Double.parseDouble(modeloTableCart.getValueAt(i, 2).toString());
        }
        DecimalFormat formato = new DecimalFormat("#.##");
        valorFinalLabel.setText("Valor final: " + "R$ " + formato.format(valorFinal).toString());
    }
    public void atualizaCardapio(){
        File cardapios = new File("default_files/cardapios.txt");
        try{
            if(cardapios.exists()){
                List<String> linhas = new ArrayList<>(Files.readAllLines(cardapios.toPath(), StandardCharsets.UTF_8));
                for(int i=0; i<modeloTableCart.getRowCount(); i++){
                    for(int j=0; j<linhas.size(); j=j+6){
                        produto.setIdProduto(Integer.parseInt(linhas.get(j)));
                        produto.setNomeProduto(linhas.get(j+1));
                        produto.setCategoria(linhas.get(j+2));
                        produto.setQuantidade(Integer.parseInt(linhas.get(j+3)));
                        produto.setQuantidadeMinima(Integer.parseInt(linhas.get(j+4)));
                        produto.setPrecoUnidade(Double.parseDouble(linhas.get(j+5)));
                        if(produto.getNomeProduto().equals(modeloTableCart.getValueAt(i, 0))){
                            int value = (int) produto.getQuantidade()- (int)Integer.parseInt(modeloTableCart.getValueAt(i, 1).toString());
                            linhas.set(j+3, String.valueOf(value));
                            break;
                        }
                    }
                }
                Files.write(cardapios.toPath(), linhas, StandardCharsets.UTF_8);
            } else{
                throw new FileNotFoundException();
            }
        } catch(FileNotFoundException ex){
            JOptionPane.showMessageDialog(null, "Arquivo 'cardapios.txt' não encontrado na pasta padrão 'default_files'.", "Erro", JOptionPane.ERROR_MESSAGE);
        } catch(IOException excessao){
            JOptionPane.showMessageDialog(null, "Arquivo 'cardapios.txt' não encontrado na pasta padrão 'default_files'.", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void atualizaTela(){
        modeloTableCart.setNumRows(0);
        valorFinalLabel.setText("Valor final: " + "R$ 0.00");
        clienteText.setText("");
        leArquivo();
        modeloTableProduto.setValueAt(produto.getIdProduto(), 0, 0);
        modeloTableProduto.setValueAt(produto.getNomeProduto(), 0, 1);
        modeloTableProduto.setValueAt(produto.getQuantidade(), 0, 2);
        modeloTableProduto.setValueAt(produto.getQuantidadeMinima(), 0, 3);
        modeloTableProduto.setValueAt(produto.getPrecoUnidade(), 0, 4);
        JOptionPane.showMessageDialog(null, "Venda realizada com SUCESSO!");
    }
}