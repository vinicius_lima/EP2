package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.pedido.Produto;
import model.usuarios.Funcionario;
import view.JanelaPrincipal;

public class JanelaCardapioClickListener implements ActionListener{
    private Funcionario funcionario;
    private JFrame mainFrame;
    private JPanel mainPanel;
    private Produto produto;
    private JTable tabela;
    private DefaultTableModel modelo;

    public JanelaCardapioClickListener(JFrame mainFrame, JPanel mainPanel, Funcionario funcionario, JTable tabela) {
        this.tabela = tabela;
        this.funcionario = funcionario;
        this.mainFrame = mainFrame;
        this.mainPanel = mainPanel;
    }

    public JanelaCardapioClickListener(DefaultTableModel modelo) {
        this.modelo = modelo;
    }

    public JanelaCardapioClickListener(JTable tabela, DefaultTableModel modelo) {
        this.tabela = tabela;
        this.modelo = modelo;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        if(comando.equals("voltar")){
            for(int i =0; i<tabela.getRowCount(); i++){
                if(!verificaEntradas(i)){
                    JOptionPane.showMessageDialog(null, "Linha: "+ String.valueOf(i+1)+". A quantidade de estoque deve ser maior que a quatidade mínima", "Erro", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
            File arquivoCardapios = new File("default_files/cardapios.txt");
            arquivoCardapios.delete();
            
            produto = new Produto();
            int linhas = 0;
            
            File cardapios = new File("default_files/cardapios.txt");
            try {
                cardapios.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(JanelaCardapioClickListener.class.getName()).log(Level.SEVERE, null, ex);
            }
            try{
                while(tabela.getRowCount() > linhas){
                    for(int colunas = 0; colunas < 6; colunas++){
                        switch (colunas) {
                            case 0:
                                produto.setIdProduto((Integer) tabela.getValueAt(linhas, colunas));
                                break;
                            case 1:
                                produto.setNomeProduto((String) tabela.getValueAt(linhas, colunas));
                                break;
                            case 2:
                                produto.setCategoria((String) tabela.getValueAt(linhas, colunas));
                                break;
                            case 3:
                                produto.setQuantidade((Integer) tabela.getValueAt(linhas, colunas));
                                break;
                            case 4:
                                produto.setQuantidadeMinima((Integer) tabela.getValueAt(linhas, colunas));
                                break;
                            case 5:
                                produto.setPrecoUnidade((Double) tabela.getValueAt(linhas, colunas));
                                break;
                            default:
                                break;
                        }
                    }
                    if(produto.getIdProduto().toString().equals("") || produto.getNomeProduto().equals("") || produto.getCategoria().equals("") || produto.getQuantidade().toString().equals("") || produto.getQuantidadeMinima().toString().equals("") || produto.getPrecoUnidade().toString().equals("")){
                        throw new NullPointerException();
                    }
                    if(cardapios.exists()){
                        FileWriter escritor = new FileWriter(cardapios, true);
                        escritor.write(produto.getIdProduto().toString() + '\n');
                        escritor.write(produto.getNomeProduto() + '\n');
                        escritor.write(produto.getCategoria() + '\n');
                        escritor.write(produto.getQuantidade().toString() + '\n');
                        escritor.write(produto.getQuantidadeMinima().toString() + '\n');
                        escritor.write(produto.getPrecoUnidade().toString() + '\n');
                        escritor.close();
                    } else {
                        throw new IOException();
                    }
                    linhas++;
                }
                JOptionPane.showMessageDialog(null, "Informações salvas com sucesso!");
                mainPanel.setVisible(false);
                mainFrame.remove(mainPanel);
            } catch(NullPointerException a){
                JOptionPane.showMessageDialog(null, "Preencha todas as cédulas da tabela!", "Erro", JOptionPane.ERROR_MESSAGE);
            } catch(IOException ex){
                JOptionPane.showMessageDialog(null, "Arquivo 'cardapios.txt' não encontrado na pasta padrão", "Erro", JOptionPane.ERROR_MESSAGE);
            } finally{
                cardapios.exists();
            }
            JanelaPrincipal janelaPrincipal = new JanelaPrincipal(mainFrame, funcionario);
        } else if(comando.equals("adicionar")){
            Vector rowData = null;
            modelo.addRow(rowData);
        } else if(comando.equals("remover")){
            int[] selectedRows = tabela.getSelectedRows();
            for(int i=0; i<selectedRows.length; i++){
                modelo.removeRow(selectedRows[i] - i);
            }
        }
    }
    public boolean verificaEntradas(int i){
        if(Integer.parseInt(tabela.getValueAt(i, 3).toString()) > Integer.parseInt(tabela.getValueAt(i, 4).toString())){
            return true;
        } else{
            return false;
        }
    }
}