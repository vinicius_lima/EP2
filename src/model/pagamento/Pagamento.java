package model.pagamento;

public class Pagamento {
    private Double valor;
    public static final String PAGAMENTO_A_VISTA = "Dinheiro";
    public static final String PAGAMENTO_CARTAO = "Cartão de Crédito";

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}