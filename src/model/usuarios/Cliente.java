package model.usuarios;


public class Cliente extends Pessoa {
    int mesa;
    String observacoes;
    
    public Cliente(String nome, int mesa, String observacoes){
        setNome(nome);
        this.mesa = mesa;
        this.observacoes = observacoes;
    }
    
    public int getMesa() {
        return mesa;
    }

    public void setMesa(int mesa) {
        this.mesa = mesa;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }
    
}
