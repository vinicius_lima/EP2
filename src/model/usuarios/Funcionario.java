
package model.usuarios;

public class Funcionario extends Pessoa{
    private String login;
    private String senha;
    
    public Funcionario(){
        this.login = null;
        this.senha = null;
        setNome(null);
    }
    
    public Funcionario(String nome, String login, String senha){
        this.login = login;
        this.senha = senha;
        setNome(nome);
    }
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
}
