package model.pedido;

import java.util.List;
import model.pagamento.Pagamento;
import model.usuarios.Cliente;
import model.usuarios.Funcionario;

public class Pedido {
    private Funcionario funcionario;
    private Cliente cliente;
    private List<Produto> produto;
    private String data;
    private String hora;
    private Pagamento formaDePagamento;
    
    public Pedido(){}


    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Produto> getProduto() {
        return produto;
    }

    public void setProduto(List<Produto> produto) {
        this.produto = produto;
    }

    public Pagamento getFormaDePagamento() {
        return formaDePagamento;
    }

    public void setFormaDePagamento(Pagamento formaDePagamento) {
        this.formaDePagamento = formaDePagamento;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}