package teste;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import model.pedido.Produto;
import org.junit.Test;
import static org.junit.Assert.*;


public class TesteSalvaProdutoArquivo {
    
    public TesteSalvaProdutoArquivo() {
    }

    @Test
    public void testeSalvaProduto() {
        File tempFile = new File("default_files/temp.txt");
        Produto produtoEscreve = new Produto(2, "Sorvete", "Sobremesa", 5, 1, 4.5);
        Produto produtoLe = new Produto();
        
        try{
            FileWriter escrito = new FileWriter(tempFile);
            escrito.write(String.valueOf(produtoEscreve.getIdProduto()) + '\n');
            escrito.write(produtoEscreve.getNomeProduto() + '\n');
            escrito.write(produtoEscreve.getCategoria() + '\n');
            escrito.write(String.valueOf(produtoEscreve.getQuantidade()) + '\n');
            escrito.write(String.valueOf(produtoEscreve.getQuantidadeMinima()) + '\n');
            escrito.write(String.valueOf(produtoEscreve.getPrecoUnidade()) + '\n');
            escrito.close();
        } catch(FileNotFoundException a){
            
        } catch(IOException ex){
            
        }
        try{
            if(tempFile.exists()){
                Scanner leitor = new Scanner(tempFile);
                String linha;
                while(leitor.hasNextLine()){
                    linha = leitor.nextLine();
                    produtoLe.setIdProduto(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produtoLe.setNomeProduto(linha);
                    linha = leitor.nextLine();
                    produtoLe.setCategoria(linha);
                    linha = leitor.nextLine();
                    produtoLe.setQuantidade(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produtoLe.setQuantidadeMinima(Integer.parseInt(linha));
                    linha = leitor.nextLine();
                    produtoLe.setPrecoUnidade(Double.parseDouble(linha));
                }
                leitor.close();
            } else {
                throw new FileNotFoundException();
            }
        } catch(FileNotFoundException ex){
            
        }
        tempFile.deleteOnExit();
        assertEquals(produtoLe.getIdProduto(), produtoEscreve.getIdProduto());
        assertEquals(produtoLe.getNomeProduto(), produtoEscreve.getNomeProduto());
        assertEquals(produtoLe.getCategoria(), produtoEscreve.getCategoria());
        assertEquals(produtoLe.getQuantidade(), produtoEscreve.getQuantidade());
        assertEquals(produtoLe.getQuantidadeMinima(), produtoEscreve.getQuantidadeMinima());
        assertEquals(produtoLe.getPrecoUnidade(), produtoEscreve.getPrecoUnidade());
    }
}
