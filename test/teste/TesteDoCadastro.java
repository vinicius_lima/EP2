package teste;

import javax.swing.JFormattedTextField;
import javax.swing.JPasswordField;
import model.usuarios.Funcionario;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteDoCadastro {
    
    public TesteDoCadastro() {
    }

    @Test
    public void testeCadastro() {
        JFormattedTextField nomeText = new JFormattedTextField(), idText = new JFormattedTextField();
        JPasswordField senhaText = new JPasswordField(), confirmaSenhaText = new JPasswordField();
        Funcionario funcionario = new Funcionario();
        
        nomeText.setText("funcionario_teste");
        idText.setText("login_teste");
        senhaText.setText("1234");
        confirmaSenhaText.setText("1234");
        funcionario.setNome(nomeText.getText());
        funcionario.setLogin(idText.getText());
        funcionario.setSenha(senhaText.getText());
       
        assertEquals(senhaText.getText(), confirmaSenhaText.getText());
        assertEquals(nomeText.getText(), funcionario.getNome());
        assertEquals(funcionario.getLogin(), idText.getText());
        assertEquals(funcionario.getSenha(), senhaText.getText());
    }
}
