package teste;

import java.util.ArrayList;
import java.util.List;
import model.pedido.Pedido;
import model.pedido.Produto;
import model.usuarios.Cliente;
import model.usuarios.Funcionario;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteDoPedido {
    
    public TesteDoPedido() {
    }

    @Test
    public void testaPedido() {
        Pedido pedido = new Pedido();
        Funcionario funcionario = new Funcionario("funcionario_teste", "login_teste", "1234");
        Cliente cliente = new Cliente("cliente_teste", 2, "");
        Produto item = new Produto(2, "Suco de Laranja", "Bebida", 3, 1, 4.5);
        List<Produto> produtos = new ArrayList<>();
        
        produtos.add(item);
        pedido.setCliente(cliente);
        pedido.setFuncionario(funcionario);
        pedido.setProduto(produtos);
        
        assertEquals(produtos, pedido.getProduto());
    }
}
