package teste;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.JFormattedTextField;
import javax.swing.JPasswordField;
import model.usuarios.Funcionario;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteDoLogin {
    
    public TesteDoLogin() {
    }

    @Test
    public void testeLogin() {
        JFormattedTextField idLogin = new JFormattedTextField();
        idLogin.setText("teste");
        JPasswordField senhaText = new JPasswordField("teste");
        Funcionario funcionario = new Funcionario("Teste Anônimo", "teste", "teste");
        Funcionario funcionarioTest = new Funcionario();
        File file = new File("default_files/cadastros.txt");
        
        try{
            if(file.exists()){
                Scanner leitor = new Scanner(file);
                String nome, id, senha;
                while(leitor.hasNextLine()){
                    nome = leitor.nextLine();
                    id = leitor.nextLine();
                    senha = leitor.nextLine();
                    if(senhaText.getText().toString().equals(senha) && idLogin.getText().toString().equals(id)){
                        funcionarioTest.setNome(nome);
                        funcionarioTest.setLogin(id);
                        funcionarioTest.setSenha(senha);
                        break;
                    }
                }
            } else{
                throw new FileNotFoundException();
            }
        } catch(FileNotFoundException ex){
            
        }
        assertEquals(funcionarioTest.getNome(), funcionario.getNome());
        assertEquals(funcionarioTest.getLogin(), funcionario.getLogin());
        assertEquals(funcionarioTest.getSenha(), funcionario.getSenha());
    }
}
